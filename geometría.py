#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"
# PROGRAMA EN DONDE SE UTILICE LAS CLASES CODIFICADAS DE LA SIGUIENTE MANERA:
# Crear los puntos A(2, 3), B(5,5), C(-3, -1) y D(0,0) e imprímalos por pantalla.
# Consultar a que cuadrante pertenecen el punto A, C y D.
# Consultar la distancia entre los puntos 'A y B' y 'B y A'.
# Determinar cual de los 3 puntos A, B o C, se encuentra más lejos del origen, punto (0,0).
# Crea un rectángulo utilizando los puntos A y B.
# Consultar la base, altura y área del rectángulo.

import math

class Punto:
    x= 0
    y= 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("{} pertenece al primer cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print("{} pertenece al segundo cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("{} pertenece al tercer cuadrante".format(self))
        elif self.x > 0 and self.y < 0:
            print("{} pertenece al cuarto cuadrante".format(self))
        elif self.x != 0 and self.y == 0:
            print("{} se sitúa sobre el eje X".format(self))
        elif self.x == 0 and self.y != 0:
            print("{} se sitúa sobre el eje Y".format(self))
        else:
            print("{} se encuentra sobre el origen".format(self))

    def vector(self, p):
        print("El vector entre {} y {} es ({}, {})".format(self, p, p.x - self.x, p.y - self.y) )

    def distancia(self, p):
        d = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2 )
        print("La distancia entre los puntos {} y {} es {}".format(self, p, d))

    def __str__(self):
        return "({}, {})".format(self.x, self.y)


class Rectángulo:
    punto_inicial=Punto
    punto_final=Punto

    def __init__(self, punto_inicial=Punto, punto_final=Punto):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final
        self.pun_base = abs(self.punto_final.x - self.punto_inicial.x)
        self.pun_altura= abs(self.punto_final.y - self.punto_inicial.y)
        self.pun_área = self.pun_base * self.pun_altura

    def base(self):
        print("La base del rectángulo es {}".format( self.pun_base ) )

    def altura(self):
        print("La altura del rectángulo es {}".format( self.pun_altura) )

    def área(self):
        print("El área del rectángulo es {}".format( self.pun_área ) )

A = Punto(2,3)
B = Punto(5,5)
C = Punto(-3, -1)
D = Punto(0,0)

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.distancia(B)
B.distancia(A)

A.distancia(D)
B.distancia(D)
C.distancia(D)

rect = Rectángulo(A, B)
rect.base()
rect.altura()
rect.área()